package com.iserveu.dmtfino.services;

import org.springframework.http.ResponseEntity;

import com.iserveu.dmtfino.responses.TransactionAPIResponse;
import com.iserveu.dmtfino.responses.StatusEnquiryAPIResponse;

public interface FinoAPIService {

	StatusEnquiryAPIResponse consumeFINOTransactionStatusEnquiryAPI(String clientUniqueId);

	ResponseEntity<TransactionAPIResponse> consumeFINOTransactionAPI(Long clientUniqueID, String customerMobileNo,
			String beneIFSCCode, String beneAccountNo, String beneName, String amount, String customerName,
			String transactionMode);

}
