package com.iserveu.dmtfino.services.impl;

import java.lang.reflect.Type;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.iserveu.dmtfino.constants.DMTConstants;
import com.iserveu.dmtfino.requests.FinoEndpointRequest;
import com.iserveu.dmtfino.responses.FinoEndpointResponse;
import com.iserveu.dmtfino.responses.StatusEnquiryAPIResponse;
import com.iserveu.dmtfino.responses.TransactionAPIResponse;
import com.iserveu.dmtfino.responses.TrimResponseData;
import com.iserveu.dmtfino.services.FinoAPIService;
import com.iserveu.dmtfino.services.FinoService;
import com.iserveu.dmtfino.utils.Utility;

@Service
public class FinoServiceImpl implements FinoService {

	static final Logger LOGGER = Logger.getLogger(FinoServiceImpl.class);

	@Autowired
	private FinoAPIService finoAPIService;

	@Override
	public FinoEndpointResponse doTransaction(FinoEndpointRequest request) {

		LOGGER.info("FINO transaction Service reuqest >>>>>>> " + request);

		ResponseEntity<TransactionAPIResponse> neftResponseWrapper = finoAPIService.consumeFINOTransactionAPI(
																					request.getClientUniqueID(), 
																					request.getCustomerMobileNo(), 
																					request.getBeneIFSCCode(),
																					request.getBeneAccountNo(), 
																					request.getBeneName(), 
																					request.getAmount(), 
																					request.getCustomerName(),
																					request.getTransactionMode());

		TransactionAPIResponse apiResponse = neftResponseWrapper.getBody();

		FinoEndpointResponse response = new FinoEndpointResponse();

		if (apiResponse.getResponseCode() != null 
				&& Integer.parseInt(apiResponse.getResponseCode()) == 0) {

			response.setMessage(apiResponse.getMessageString() == null ? "" : apiResponse.getMessageString());
			response.setClientUniqueId(apiResponse.getClientUniqueID() == null ? "" : apiResponse.getClientUniqueID());

			Type collectiontype = new TypeToken<TrimResponseData>() {
			}.getType();
			TrimResponseData trimmedData = (TrimResponseData) new Gson().fromJson(apiResponse.getResponseData(),
					collectiontype);

			response.setRrn(trimmedData.getTxnID() == null ? "" : trimmedData.getTxnID());
			response.setAmount(trimmedData.getAmountRequested() == null ? "" : trimmedData.getAmountRequested());
			response.setChargesDeducted(trimmedData.getChargesDeducted() == null ? "" : trimmedData.getChargesDeducted());
			response.setTotalAmount(trimmedData.getTotalAmount() == null ? "" : trimmedData.getTotalAmount());
			response.setBeneName(trimmedData.getBeneName() == null ? "" : trimmedData.getBeneName());
			response.setTransactionDateAndTime(trimmedData.getTransactionDatetime() == null ? "" : trimmedData.getTransactionDatetime());

			if (trimmedData.getActCode().equals("0")) {

				response.setStatusCode(0);
				response.setStatus(DMTConstants.STATUS_SUCCESS);
				response.setMessage(
						Utility.responseCodeDesc(trimmedData.getActCode() != null ? trimmedData.getActCode() : ""));

			} else if ((trimmedData.getActCode().equals("62")) || (trimmedData.getActCode().equals("96"))) {

				response.setStatusCode(1);
				response.setStatus(DMTConstants.STATUS_INPROGRESS);
				response.setMessage(
						Utility.pendingStatusDesc(trimmedData.getActCode() != null ? trimmedData.getActCode() : "")
								+ ":" + trimmedData.getActCode());
			} else {

				response.setStatusCode(1);
				response.setStatus(DMTConstants.STATUS_INPROGRESS);
				response.setMessage(
						Utility.responseCodeDesc(trimmedData.getActCode() != null ? trimmedData.getActCode() : "") + ":"
								+ trimmedData.getActCode());
			}

			return response;

		} else if (apiResponse.getResponseCode() != null 
				&& Integer.parseInt(apiResponse.getResponseCode()) == -6) {

			response.setStatusCode(1);
			response.setStatus(DMTConstants.STATUS_INPROGRESS);
			response.setMessage(apiResponse.getMessageString() == null ? "" : apiResponse.getMessageString());
			response.setClientUniqueId(apiResponse.getClientUniqueID() == null ? "" : apiResponse.getClientUniqueID());

			Type collectiontype = new TypeToken<TrimResponseData>() {
			}.getType();

			TrimResponseData trimmedData = (TrimResponseData) new Gson().fromJson(apiResponse.getResponseData(),
					collectiontype);

			response.setMessage(Utility.responseCodeDesc(trimmedData.getActCode() != null ? trimmedData.getActCode() : ""));
			response.setRrn(trimmedData.getTxnID() == null ? "" : trimmedData.getTxnID());
			response.setAmount(trimmedData.getAmountRequested() == null ? "" : trimmedData.getAmountRequested());
			response.setChargesDeducted(trimmedData.getChargesDeducted() == null ? "" : trimmedData.getChargesDeducted());
			response.setTotalAmount(trimmedData.getTotalAmount() == null ? "" : trimmedData.getTotalAmount());
			response.setBeneName(trimmedData.getBeneName() == null ? "" : trimmedData.getBeneName());
			response.setTransactionDateAndTime(trimmedData.getTransactionDatetime() == null ? "" : trimmedData.getTransactionDatetime());

			return response;

		} else {

			response.setStatusCode(-1);
			response.setStatus(DMTConstants.STATUS_FAILED);

			if ((null != apiResponse.getMessageString())
					&& (apiResponse.getMessageString().contains("Insufficient balance on account"))) {

				response.setMessage("Service Unavailable");
				response.setStatusDesc("Please contact customer support");

			} else {

				response.setMessage(apiResponse.getMessageString() == null ? "" : apiResponse.getMessageString());
				response.setStatusDesc(apiResponse.getDisplayMessage() == null ? "" : apiResponse.getDisplayMessage());

			}

			response.setClientUniqueId(apiResponse.getClientUniqueID() == null ? "" : apiResponse.getClientUniqueID());

			return response;

		}

	}

	@Override
	public FinoEndpointResponse doStatusEnquiry(String clientUniqueId) {

		LOGGER.info("FINO transactionstatus enquiry Service reuqest >>>>>>> " + clientUniqueId);
		StatusEnquiryAPIResponse txnStatusWrapper = finoAPIService.consumeFINOTransactionStatusEnquiryAPI(clientUniqueId);
		
		FinoEndpointResponse response = new FinoEndpointResponse();
		
		if (txnStatusWrapper.getResponseCode().equals("0")) {
			
			response.setMessage(txnStatusWrapper.getMessageString() == null ? "" : txnStatusWrapper.getMessageString());
			response.setStatusDesc(txnStatusWrapper.getMessageString() == null ? "" : txnStatusWrapper.getMessageString());
			response.setClientUniqueId(txnStatusWrapper.getClientUniqueID() == null ? "" : txnStatusWrapper.getClientUniqueID());

			Type collectiontype = new TypeToken<TrimResponseData>() { }.getType();
			TrimResponseData trimmedData = (TrimResponseData) new Gson().fromJson(txnStatusWrapper.getResponseData(), collectiontype);

			response.setRrn(trimmedData.getTxnID() == null ? "" : trimmedData.getTxnID());
			response.setAmount(trimmedData.getAmountRequested() == null ? "" : trimmedData.getAmountRequested());
			response.setChargesDeducted(trimmedData.getChargesDeducted() == null ? "" : trimmedData.getChargesDeducted());
			response.setTotalAmount(trimmedData.getTotalAmount() == null ? "" : trimmedData.getTotalAmount());
			response.setBeneName(trimmedData.getBeneName() == null ? "" : trimmedData.getBeneName());
			response.setTransactionDateAndTime(trimmedData.getTransactionDatetime() == null ? "" : trimmedData.getTransactionDatetime());

			if ((trimmedData.getActCode().equals("26")) 
					|| (trimmedData.getActCode().equals("11"))
					|| trimmedData.getActCode().equalsIgnoreCase("S")) {
				
				response.setStatusCode(0);
				response.setStatus(DMTConstants.STATUS_SUCCESS);
				response.setBeneName(trimmedData.getBeneName());
				
			} else if ((trimmedData.getActCode().equals("100")) 
					|| (trimmedData.getActCode().equals("23"))
					|| (trimmedData.getActCode().equals("21")) 
					|| (trimmedData.getActCode().equals("12"))
					|| (trimmedData.getActCode().equals("R") )
					||(trimmedData.getActCode().equals("1014"))) {
				
				response.setStatusCode(-1);
				response.setStatus(DMTConstants.STATUS_FAILED);
				
			} else {
				
				response.setStatusCode(1);
				response.setStatus(DMTConstants.STATUS_INPROGRESS);

			}

		} else {
			
			LOGGER.info(">>>>>>>>>> Unknown response got from Bank");
			response.setResponseCode(txnStatusWrapper.getResponseCode() == null ? "" : txnStatusWrapper.getResponseCode());
			response.setMessage(txnStatusWrapper.getMessageString() == null ? "" : txnStatusWrapper.getMessageString());
			response.setStatusDesc(txnStatusWrapper.getDisplayMessage() == null ? "" : txnStatusWrapper.getDisplayMessage());
			response.setClientUniqueId(txnStatusWrapper.getClientUniqueID() == null ? "" : txnStatusWrapper.getClientUniqueID());
			
			response.setStatusCode(1);
			response.setStatus(DMTConstants.STATUS_UNKNOWN);
		}
		
		return response;
		
	}

}
