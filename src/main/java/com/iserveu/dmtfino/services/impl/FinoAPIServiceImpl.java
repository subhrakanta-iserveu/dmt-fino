package com.iserveu.dmtfino.services.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.apache.commons.ssl.OpenSSL;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.iserveu.dmtfino.responses.TransactionAPIResponse;
import com.iserveu.dmtfino.responses.TrimResponseData;
import com.iserveu.dmtfino.responses.TxnStatusResponse;
import com.iserveu.dmtfino.responses.StatusEnquiryAPIResponse;
import com.iserveu.dmtfino.services.FinoAPIService;
import com.iserveu.dmtfino.utils.Utility;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;
import okio.BufferedSource;

@Service
public class FinoAPIServiceImpl implements FinoAPIService {
	
	static final Logger LOGGER = Logger.getLogger(FinoAPIServiceImpl.class);

	@Autowired
	private Environment environment;
	
	public static String convert(InputStream inputStream, Charset charset) throws IOException {
		return IOUtils.toString(inputStream, charset);
	}
	
	@Override
	public StatusEnquiryAPIResponse consumeFINOTransactionStatusEnquiryAPI(String clientUniqueId) {


		StatusEnquiryAPIResponse responseJson = new StatusEnquiryAPIResponse();
		
		try {

			ObjectMapper mapperObj = new ObjectMapper();

			InputStream headerstream = new ByteArrayInputStream(environment.getProperty("FINO_REQUEST_HEADER").getBytes());
			InputStream headerencrypted = OpenSSL.encrypt("AES256", environment.getProperty("FINO_REQUEST_HEADER_KEY").getBytes("UTF-8"), headerstream, true);

			MultiValueMap<String, String> formVars = new LinkedMultiValueMap<>();
			formVars.add("ClientUniqueID", clientUniqueId);

			String txnRequestPacket = mapperObj.writeValueAsString(formVars).replaceAll("\\[", "").replaceAll("\\]",
					"");

			InputStream txnStatusStream = new ByteArrayInputStream(txnRequestPacket.getBytes(Charset.forName("UTF-8")));
			InputStream statusencrypted = OpenSSL.encrypt("AES256", environment.getProperty("FINO_REQUEST_BODY_KEY").getBytes(), txnStatusStream, true);

			String paramValue = (convert(statusencrypted, Charset.forName("UTF-8")).replaceAll("\\s", ""));

			// Staging
			// String uri = environment.getProperty("FINO_BASE_URL") + "/txnstatusrequest";

			// Production
			String uri = environment.getProperty("FINO_BASE_URL") + "/TxnStatusRequest";

			String headerValue = convert(headerencrypted, Charset.forName("UTF-8")).replaceAll("\\s", "");
			String txnparam = "\"" + paramValue + "\"";

			Builder b = new Builder();
			b.readTimeout(90000, TimeUnit.MILLISECONDS);
			b.connectTimeout(90000, TimeUnit.MILLISECONDS);
			OkHttpClient client = b.build();

			MediaType mediaType = MediaType.parse("application/json");
			RequestBody body = RequestBody.create(mediaType, txnparam);
			Request request = new Request.Builder().url(uri).post(body).addHeader("authentication", headerValue)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache").build();

			Response response = client.newCall(request).execute();

			BufferedSource source = response.body().source();
			source.request(Long.MAX_VALUE); // Buffer the entire body.
			Buffer buffer = source.buffer();
			String responseBodyString = buffer.clone().readString(Charset.forName("UTF-8"));

			Type collectiontype = new TypeToken<StatusEnquiryAPIResponse>() {
			}.getType();
			responseJson = (StatusEnquiryAPIResponse) new Gson().fromJson(responseBodyString, collectiontype);

			InputStream responseStream = new ByteArrayInputStream(responseJson.getResponseData().getBytes());
			InputStream responsedecrypted = OpenSSL.decrypt("AES256", environment.getProperty("FINO_REQUEST_BODY_KEY").getBytes("UTF-8"), responseStream);

			responseJson.setResponseData(convert(responsedecrypted, Charset.forName("UTF-8")).replaceAll("\\s", ""));


		} catch (Exception exception) {

			//TODO: Will decide whether to give response as Pending or Failed
			responseJson.setResponseCode("-1");
			responseJson.setDisplayMessage("Exception occured FINO status enquiry API, saying - " + exception.getMessage());
			responseJson.setMessageString("Exception occured FINO status enquiry API, saying - " + exception.getMessage());
			responseJson.setClientUniqueID(clientUniqueId);
			return responseJson;

		}
		
		LOGGER.info("FINO Bank status enquiry API response >>>>>>>>>>> " + responseJson.toString());
		return responseJson;
		
	
	}

	@Override
	public ResponseEntity<TransactionAPIResponse> consumeFINOTransactionAPI(Long clientUniqueID, String customerMobileNo,
			String beneIFSCCode, String beneAccountNo, String beneName, String amount, String customerName,
			String transactionMode) {
		
		TransactionAPIResponse responseJson;

		try {
			ObjectMapper mapperObj = new ObjectMapper();

			MultiValueMap<String, String> formVars = new LinkedMultiValueMap<>();
			formVars.add("ClientUniqueID", clientUniqueID.toString());
			formVars.add("CustomerMobileNo", customerMobileNo);
			formVars.add("BeneIFSCCode", beneIFSCCode);
			formVars.add("BeneAccountNo", beneAccountNo);
			formVars.add("BeneName", beneName);
			formVars.add("Amount", amount);
			formVars.add("CustomerName", customerName);

			String neftRequestPacket = mapperObj.writeValueAsString(formVars)
												.replaceAll("\\[", "")
												.replaceAll("\\]", "");

			InputStream neftstream = new ByteArrayInputStream(neftRequestPacket.getBytes(Charset.forName("UTF-8")));
			InputStream bodyencrypted = OpenSSL.encrypt("AES256", environment.getProperty("FINO_REQUEST_BODY_KEY").getBytes(), neftstream, true);

			String uri;

			// Staging
			// if (transactionMode.equalsIgnoreCase("IMPS"))
			// uri = environment.getProperty("FINO_BASE_URL") + "/impsrequest";
			// else
			// uri = environment.getProperty("FINO_BASE_URL") + "/neftrequest";

			// Production
			if (transactionMode.equalsIgnoreCase("IMPS"))
				uri = environment.getProperty("FINO_BASE_URL") + "/IMPSRequest";
			else
				uri = environment.getProperty("FINO_BASE_URL") + "/NEFTRequest";

			String paramValue = (convert(bodyencrypted, Charset.forName("UTF-8")).replaceAll("\\s", ""));

			InputStream headerstream = new ByteArrayInputStream(environment.getProperty("FINO_REQUEST_HEADER").getBytes());
			InputStream headerencrypted = OpenSSL.encrypt("AES256", environment.getProperty("FINO_REQUEST_HEADER_KEY").getBytes("UTF-8"), headerstream, true);
			String headerValue = convert(headerencrypted, Charset.forName("UTF-8")).replaceAll("\\s", "");

			String neftparam = "\"" + paramValue + "\"";

			Builder b = new Builder();
			b.readTimeout(90000, TimeUnit.MILLISECONDS);
			b.connectTimeout(90000, TimeUnit.MILLISECONDS);
			OkHttpClient client = b.build();

			MediaType mediaType = MediaType.parse("application/json");
			RequestBody body = RequestBody.create(mediaType, neftparam);
			
			Request request = new Request.Builder()
											.url(uri)
											.post(body)
											.addHeader("authentication", headerValue)
											.addHeader("content-type", "application/json")
											.addHeader("cache-control", "no-cache")
											.build();
			
			LOGGER.info("FINO Bank API Request >>>>>>>>>> " + neftRequestPacket + ", Consumed URl >>>>>>>> " + uri);

			Response response = client.newCall(request).execute();
			BufferedSource source = response.body().source();
			source.request(Long.MAX_VALUE); // Buffer the entire body.
			Buffer buffer = source.buffer();
			String responseBodyString = buffer.clone().readString(Charset.forName("UTF-8"));
			Type collectiontype = new TypeToken<TransactionAPIResponse>() {
			}.getType();
			responseJson = (TransactionAPIResponse) new Gson().fromJson(responseBodyString, collectiontype);

			InputStream responseStream = new ByteArrayInputStream(responseJson.getResponseData().getBytes());
			InputStream responsedecrypted = OpenSSL.decrypt("AES256", environment.getProperty("FINO_REQUEST_BODY_KEY").getBytes("UTF-8"), responseStream);

			responseJson.setResponseData(
					convert(responsedecrypted, Charset.forName("UTF-8")).replaceAll("\\s", "").toString());

			responseJson.setClientUniqueID(clientUniqueID.toString());

			LOGGER.info("FINO Bank API response >>>>>>>>>>> " + responseJson.toString());

			return new ResponseEntity<>(responseJson, HttpStatus.OK);

		} catch (Exception e) {

			LOGGER.info(">>>>>>>>>> Exception occured at consuming bank API saying - " + e.getMessage());
			responseJson = new TransactionAPIResponse();

			try {

				TxnStatusResponse statusResponse = getTransactionStatus(clientUniqueID.toString());

				responseJson.setDisplayMessage(
						statusResponse.getDisplayMessage() == null ? e.getMessage() : statusResponse.getDisplayMessage());
				responseJson.setClientUniqueID(clientUniqueID.toString() == null ? "" : clientUniqueID.toString());
				responseJson.setMessageString(
						statusResponse.getMessageString() == e.getMessage() ? "" : statusResponse.getMessageString());
				responseJson.setRequestID(statusResponse.getRequestID() == null ? "" : statusResponse.getRequestID());
				responseJson.setResponseData(
						statusResponse.getResponseData() == null ? "" : statusResponse.getResponseData());
				if(statusResponse.getStatusCode()==-6)
				{
					responseJson.setResponseCode("-6");
				}

			} catch (Exception e2) {
				
				responseJson.setMessageString(e2.getMessage());
				responseJson.setDisplayMessage(e2.getMessage());
				LOGGER.info(">>>>>>>> Exception occured at consuming bank API saying - " + e2.getMessage());
				return new ResponseEntity<>(responseJson, HttpStatus.BAD_REQUEST);
			}
			
			return new ResponseEntity<>(responseJson, HttpStatus.OK);

		}
		
	}
	
	public TxnStatusResponse getTransactionStatus(String clientUniqueID) {
		TxnStatusResponse status;
		try {
			
			StatusEnquiryAPIResponse statusResponse = consumeFINOTransactionStatusEnquiryAPI(clientUniqueID);

			if (statusResponse.getResponseCode().equals("0")) {
				status = new TxnStatusResponse();

				status.setResponseCode(
						statusResponse.getResponseCode() == null ? "" : statusResponse.getResponseCode());
				status.setMessageString(
						statusResponse.getMessageString() == null ? "" : statusResponse.getMessageString());
				status.setDisplayMessage(
						statusResponse.getDisplayMessage() == null ? "" : statusResponse.getDisplayMessage());
				status.setClientUniqueID(
						statusResponse.getClientUniqueID() == null ? "" : statusResponse.getClientUniqueID());
				status.setRequestID(statusResponse.getRequestID() == null ? "" : statusResponse.getRequestID());

				status.setResponseData(
						statusResponse.getResponseData() == null ? "" : statusResponse.getResponseData());

				Type collectiontype = new TypeToken<TrimResponseData>() {
				}.getType();
				TrimResponseData trimmedData = (TrimResponseData) new Gson().fromJson(statusResponse.getResponseData(),
						collectiontype);

				status.setActCode(trimmedData.getActCode() == null ? "" : trimmedData.getActCode());
				status.setTxnID(trimmedData.getTxnID() == null ? "" : trimmedData.getTxnID());
				status.setAmountRequested(
						trimmedData.getAmountRequested() == null ? "" : trimmedData.getAmountRequested());
				status.setChargesDeducted(
						trimmedData.getChargesDeducted() == null ? "" : trimmedData.getChargesDeducted());
				status.setTotalAmount(trimmedData.getTotalAmount() == null ? "" : trimmedData.getChargesDeducted());
				status.setBeneName(trimmedData.getBeneName() == null ? "" : trimmedData.getBeneName());
				status.setRfu1(trimmedData.getRfu1() == null ? "" : trimmedData.getRfu1());
				status.setRfu2(trimmedData.getRfu2() == null ? "" : trimmedData.getRfu2());
				status.setRfu3(trimmedData.getRfu3() == null ? "" : trimmedData.getRfu3());
				status.setTransactionDatetime(
						trimmedData.getTransactionDatetime() == null ? "" : trimmedData.getTransactionDatetime());
				status.setTxnDescription(
						trimmedData.getTxnDescription() == null ? "" : trimmedData.getTxnDescription());

				if ((trimmedData.getActCode().equals("26")) || (trimmedData.getActCode().equals("11"))
						|| trimmedData.getActCode().equalsIgnoreCase("S")) {
					status.setStatusCode(0);
					status.setStatus("Success");
					status.setBeneName(trimmedData.getBeneName());
					status.setMessage(Utility.
							impsTxnStatusCodeDesc(trimmedData.getActCode() != null ? trimmedData.getActCode() : ""));
				} else if ((trimmedData.getActCode().equals("100")) || (trimmedData.getActCode().equals("23"))
						|| (trimmedData.getActCode().equals("21")) || (trimmedData.getActCode().equals("12"))
						|| (trimmedData.getActCode().equals("R") )||(trimmedData.getActCode().equals("1014"))) {
					status.setStatusCode(2);
					status.setStatus("Failure");
					status.setMessage(Utility.
							impsTxnStatusCodeDesc(trimmedData.getActCode() != null ? trimmedData.getActCode() : ""));
				} else {
					status.setStatusCode(1);
					status.setStatus("Pending");
					status.setMessage(Utility.
							impsTxnStatusCodeDesc(trimmedData.getActCode() != null ? trimmedData.getActCode() : ""));

				}

				return status;
			}

			else {

				status = new TxnStatusResponse();

				status.setResponseCode(
						statusResponse.getResponseCode() == null ? "" : statusResponse.getResponseCode());
				status.setMessageString(
						statusResponse.getMessageString() == null ? "" : statusResponse.getMessageString());
				status.setDisplayMessage(
						statusResponse.getDisplayMessage() == null ? "" : statusResponse.getDisplayMessage());
				status.setClientUniqueID(
						statusResponse.getClientUniqueID() == null ? "" : statusResponse.getClientUniqueID());
				status.setRequestID(statusResponse.getRequestID() == null ? "" : statusResponse.getRequestID());

				status.setResponseData(
						statusResponse.getResponseData() == null ? "" : statusResponse.getResponseData());

				Type collectiontype = new TypeToken<TrimResponseData>() {
				}.getType();
				TrimResponseData trimmedData = (TrimResponseData) new Gson().fromJson(statusResponse.getResponseData(),
						collectiontype);

				status.setActCode(trimmedData.getActCode() == null ? "" : trimmedData.getActCode());
				status.setTxnID(trimmedData.getTxnID() == null ? "" : trimmedData.getTxnID());
				status.setAmountRequested(
						trimmedData.getAmountRequested() == null ? "" : trimmedData.getAmountRequested());
				status.setChargesDeducted(
						trimmedData.getChargesDeducted() == null ? "" : trimmedData.getChargesDeducted());
				status.setTotalAmount(trimmedData.getTotalAmount() == null ? "" : trimmedData.getChargesDeducted());
				status.setBeneName(trimmedData.getBeneName() == null ? "" : trimmedData.getBeneName());
				status.setRfu1(trimmedData.getRfu1() == null ? "" : trimmedData.getRfu1());
				status.setRfu2(trimmedData.getRfu2() == null ? "" : trimmedData.getRfu2());
				status.setRfu3(trimmedData.getRfu3() == null ? "" : trimmedData.getRfu3());
				status.setTransactionDatetime(
						trimmedData.getTransactionDatetime() == null ? "" : trimmedData.getTransactionDatetime());
				status.setTxnDescription(
						trimmedData.getTxnDescription() == null ? "" : trimmedData.getTxnDescription());

				status.setStatusCode(-6);
				status.setStatus("Transaction Enquiry Failed.");
				status.setMessage(Utility.
						impsTxnStatusCodeDesc(trimmedData.getActCode() != null ? trimmedData.getActCode() : ""));

			}

			return status;

		} catch (Exception e) {

			status = new TxnStatusResponse();
			status.setStatusCode(-1);
			status.setStatus("Exception occured");
			return status;
		}
	}

}
