package com.iserveu.dmtfino.services;

import com.iserveu.dmtfino.requests.FinoEndpointRequest;
import com.iserveu.dmtfino.responses.FinoEndpointResponse;

public interface FinoService {

	FinoEndpointResponse doTransaction(FinoEndpointRequest request);

	FinoEndpointResponse doStatusEnquiry(String clientUniqueId);

}
