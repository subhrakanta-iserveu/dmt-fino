package com.iserveu.dmtfino;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DmtFinoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DmtFinoApplication.class, args);
	}

}
