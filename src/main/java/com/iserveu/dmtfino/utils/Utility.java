package com.iserveu.dmtfino.utils;

public class Utility {

	public static String responseCodeDesc(String actcode) {
		switch (actcode) {
		case "0":
			return "Transaction Successful";
		case "1":
			return "Invalid Transaction or Transaction Failed";
		case "100000":
			return "There's no IFSC defined for NEFT at branch ";
		case "3":
			return "Invalid Merchant";
		case "4":
			return "Restricted Card";
		case "05":
			return "General denial";
		case "08":
			return "Fund transfer request is timed out. Your transaction is Under Process";
		case "12":
			return "Transaction failed due to Invalid transaction type";
		case "13":
			return "Invalid Amount Field";
		case "14":
			return "Invalid Account Number";
		case "1515":
			return "Invalid Beneficiary IFSC";
		case "17":
			return "Deposit Timeout/Cancel";
		case "20":
			return "Transaction failed due to Invalid response code";
		case "22":
			return "Incomplete Transaction";
		case "2388":
			return "Required fields cannot be null";
		case "30":
			return "Invalid Message";
		case "3098":
			return "IMPS Rej - exceeds Daily Transaction Limit";
		case "3247":
			return "Access not allowed for MRPC at this time";
		case "401":
			return "Full authentication is required to access this resource.";
		case "500":
			return "timeout";
		case "51":
			return "Transaction not allowed Please try again after sometime";
		case "52":
			return "Invalid Account";
		case "54":
			return "Expired";
		case "61":
			return "Transfer Amount Exceeds Limit";
		case "62":
			return "Npci Node Down";
		case "64":
			return "Maximum Amount Exceeded";
		case "65":
			return "Transaction Not Found";
		case "700292":
			return "Transaction Amount exceeds the defined Limit";
		case "700322":
			return "FINOMB IMPSBENV parameter not defined in ZUTBLIMPAR or NBIN not defined Beneficiary IFSC code";
		case "700420":
			return "Transaction reference no is already processed";
		case "700511":
			return "This transaction cannot be processed as your daily transaction limit has been exceeded.";
		case "91":
			return "Time out at NPCI";
		case "92":
			return "Transaction failed due to Invalid IFSC Code";
		case "94":
			return "Duplicate Transaction";
		case "96":
			return "Unable to process";
		case "99":
			return "Internal Error Occurred";
		case "998":
			return "Time Out From CBS";
		case "999":
			return "Response TimeOut";
		case "M0":
			return "Transaction Pending";
		case "M1":
			return "Fund transfer is failed due to invalid beneficiary account number";
		case "M2":
			return "Fund transfer is failed due to Amount limit exceeded for the beneficiary";
		case "M3":
			return "Fund transfer is failed due to frozen account of beneficiary";
		case "M4":
			return "Fund transfer is failed due to NRE account of beneficiary";
		case "M5":
			return "Fund transfer is failed due to account is closed";
		case "MP":
			return "Fund transfer failed as Bank not live on IMPS account / ifsc based fund transfer";
		case "9999":
			return "Txn Posting Failed";
		case "100030":
			return "Insufficient balance on account ";
		case "503":
			return "The remote server returned an error";
		case "504":
			return "The remote server returned an error";
		case "5001":
			return "Unknown Response";
		case "100050":
			return "The Bank Code is not active";
		case "3599":
			return "Must enter cost center for GL";
		case "7774":
			return "Invalid account number";
		case "-1":
			return "Exception";
		case "-5":
			return "Duplicate Transaction";
		case "1003":
			return "Header Invalid";
		case "1002":
			return "Authentication Failed";
		case "1004":
			return "Token Generation Failed";
		case "1006":
			return "Invalid Product Code";
		case "1007":
			return "Profile Data Not Found";
		case "1010":
			return "Invalid Request";
		case "1011":
			return "Invalid URL";
		case "1008":
			return "InvalidTxnAmount";
		case "1009":
			return "RequestIsNull";
		case "1012":
			return "InvalidResponse";
		case "57":
			return "Transaction Pending";
		case "56":
			return "Transaction Pending";
		case "1013":
			return "IMPS service is disable for this bank.";
		case "Z4":
			return "Transaction Pending";
		case "97":
			return "Transaction Pending";
		case "NO":
			return "Transaction Pending";
		case "39":
			return "Transaction Pending";
		case "997":
			return "No NBIN found For IFSC Code";
		case "42":
			return "Transaction Pending";
		case "86":
			return "Transaction Pending";
		case "MA":
			return "Transaction Pending";
		default:
			return "unknown error";
		}
	}

	public static String pendingStatusDesc(String actcode) {
		switch (actcode) {

		case "62":
			return "Transaction Pending";
		case "96":
			return "Transaction failed please try again after sometime";
		default:
			return "unknown error";
		}
	}
	
	public static String impsTxnStatusCodeDesc(String actcode) {
		switch (actcode) {
		case "1":
			return "Transaction Pending";
		case "5001":
			return "Unknown";
		case "2":
			return "Transaction is Pending";
		case "7":
			return "Transaction is Pending";
		case "9":
			return "Transaction is Pending";
		case "11":
			return "Transaction Successful";
		case "12":
			return "Transaction Failed";
		case "20":
			return "Transaction is Pending";
		case "21":
			return "Transaction Failed";
		case "23":
			return "Transaction Failed";
		case "26":
			return "Transaction Successful";
		case "200":
			return "Exception";
		case "401":
			return "Full authentication is required to access this resource.";
		case "100":
			return "Record Not found";
		case "R":
			return "Transaction Failed";
		case "S":
			return "Transaction Successful";
		case "P":
			return "Transaction is Pending";
		case "1010":
			return "InvalidRequest";
		case "1002":
			return "AuthenticationFailed";
		case "1004":
			return "TokenGenerationFailed";
		case "999":
			return "TokenExpired";
		case "1009":
			return "RequestIsNull";
		case "1011":
			return "URLNotFound";
		case "1012":
			return "InvalidResponse";
		case "9999":
			return "Txn Posting Failed";
		case "503":
			return "The remote server returned an error";
		case "504":
			return "The remote server returned an error";
		default:
			return "unknown error";
		}
	}
	
}
