package com.iserveu.dmtfino.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TransactionAPIResponse {
	
	private String ResponseCode;
	private String MessageString;
	private String DisplayMessage;
	private String RequestID;
	private String ClientUniqueID;
	private String ResponseData;
	
	//Added for IBL
	private String transactionType;
	private String statusDesc;
	private String iBLRefNo;//transaction id
	private int amount;
	private double IBLamount;
	private Long customerRefNo;//client unique id
	private String statusCode;
	private String beneName;
	private String responsedate;
	private String utr;
	
	//Added for RBL
	private String remarks;
	private String status;
	private String npciResponseCode;
	private String errorCode;
	private String errorDesc;
	private String bankRefNo;
	private String channelPartnerRefNo;

}
