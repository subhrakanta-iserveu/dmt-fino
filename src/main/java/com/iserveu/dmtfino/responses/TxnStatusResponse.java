package com.iserveu.dmtfino.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TxnStatusResponse {

	private String responseCode;
	private String messageString;
	private String displayMessage;
	private String requestID;
	private String clientUniqueID;
	private String responseData;
	private String message;
	private String status;
	private long statusCode;
	private String ActCode;
	private String txnID;
	private String AmountRequested;
	private String ChargesDeducted;
	private String TotalAmount;
	private String BeneName;
	private String Rfu1;
	private String Rfu2;
	private String Rfu3;
	private String TransactionDatetime;
	private String TxnDescription;


}
