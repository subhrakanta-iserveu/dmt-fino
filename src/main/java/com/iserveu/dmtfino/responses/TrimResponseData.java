package com.iserveu.dmtfino.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TrimResponseData {
	
	private String ActCode;
	private String TxnID;
	private String AmountRequested;
	private String ChargesDeducted;
	private String TotalAmount;
	private String BeneName;
	private String Rfu1;
	private String Rfu2;
	private String Rfu3;
	private String TransactionDatetime;
	private String TxnDescription;
	
}