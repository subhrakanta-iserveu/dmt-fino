package com.iserveu.dmtfino.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StatusEnquiryAPIResponse {
	
	private String ResponseCode;
	private String MessageString;
	private String DisplayMessage;
	private String RequestID;
	private String ClientUniqueID;
	private String ResponseData;
	
}
