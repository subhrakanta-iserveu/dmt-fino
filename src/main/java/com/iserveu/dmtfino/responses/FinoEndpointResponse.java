package com.iserveu.dmtfino.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FinoEndpointResponse {
	
	private int statusCode;
	private String status;
	private String statusDesc;
	private String clientUniqueId;
	private String rrn;
	private String beneName;
	private String amount;
	private String transactionType;
	private String bankRefNo;
	private String chargesDeducted;
	private String totalAmount;
	private String transactionDateAndTime;
	private String message;
	private String responseCode;
	
}
