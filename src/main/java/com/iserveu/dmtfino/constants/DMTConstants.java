package com.iserveu.dmtfino.constants;

public class DMTConstants {

	public static final String STATUS_INPROGRESS = "INPROGRESS";
	public static final String STATUS_SUCCESS = "SUCCESS";
	public static final String STATUS_FAILED = "FAILED";
	public static final String STATUS_UNKNOWN = "Unknown response";

}
