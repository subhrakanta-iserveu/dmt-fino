package com.iserveu.dmtfino.endpoints;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.iserveu.dmtfino.requests.FinoEndpointRequest;
import com.iserveu.dmtfino.responses.FinoEndpointResponse;
import com.iserveu.dmtfino.responses.StatusResponse;
import com.iserveu.dmtfino.services.FinoService;

@RestController
@CrossOrigin
public class FinoController {
	
	static final Logger LOGGER = Logger.getLogger(FinoController.class);
	
	@Autowired
	private FinoService finoService;
	
	@GetMapping("/")
	private ResponseEntity<?> welcome() {
		return new ResponseEntity<>("Welcome to DMT FINO API application.", HttpStatus.OK);
	}
	
	@PostMapping("/fino/transaction")
	public ResponseEntity<?> doTransaction(@RequestBody @Valid FinoEndpointRequest request,
											BindingResult result) {
		
		if (result.hasErrors()) {
			
			LOGGER.error("Validation Error >>>>>>>> " + result.getFieldError().getDefaultMessage());
			return new ResponseEntity<>(new StatusResponse(-1, result.getFieldError().getDefaultMessage()), 
											HttpStatus.BAD_REQUEST);
		}
		
		LOGGER.info("FINO transaction API reuqest >>>>>>> " + request);
		
		try {
			
			FinoEndpointResponse response = finoService.doTransaction(request);
			
			LOGGER.info("FINO transaction response >>>>>>> " + response);
			return new ResponseEntity<>(response, HttpStatus.OK);
			
		} catch(Exception exception) {
			
			LOGGER.info(">>>>>>>> Custom exception, saying - " + exception.getMessage());
			return new ResponseEntity<>(new StatusResponse(-1, "Custom exception, saying - " + exception.getMessage()), 
											HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
	}
	
	@PostMapping("/fino/verify-bene")
	public ResponseEntity<?> doBeneficiaryVerification(@RequestBody @Valid FinoEndpointRequest request,
															BindingResult result) {
		
		if (result.hasErrors()) {
			
			LOGGER.error("Validation Error >>>>>>>> " + result.getFieldError().getDefaultMessage());
			return new ResponseEntity<>(new StatusResponse(-1, result.getFieldError().getDefaultMessage()), 
											HttpStatus.BAD_REQUEST);
		}
		
		LOGGER.info("FINO beneficiary verification API reuqest >>>>>>> " + request);
		
		try {
			
			FinoEndpointResponse response = finoService.doTransaction(request);
			LOGGER.info("FINO beneficiary verification response >>>>>>> " + response);
			return new ResponseEntity<>(response, HttpStatus.OK);
			
		} catch(Exception exception) {
			
			LOGGER.info(">>>>>>>> Custom exception, saying - " + exception.getMessage());
			return new ResponseEntity<>(new StatusResponse(-1, "Custom exception, saying - " + exception.getMessage()), 
											HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
	}
	
	@PostMapping("/fino/status-enquiry/{clientUniqueId}")
	public ResponseEntity<?> doStatusEnquiry(@PathVariable String clientUniqueId) {
		
		LOGGER.info("FINO Status Enquiry API reuqest >>>>>>> " + clientUniqueId);
		
		try {
			
			FinoEndpointResponse response = finoService.doStatusEnquiry(clientUniqueId);
			LOGGER.info("FINO Status Enquiry response >>>>>>> " + response);
			return new ResponseEntity<>(response, HttpStatus.OK);
			
		} catch(Exception exception) {
			
			LOGGER.info(">>>>>>>> Custom exception, saying - " + exception.getMessage());
			return new ResponseEntity<>(new StatusResponse(-1, "Custom exception, saying - " + exception.getMessage()), 
											HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
	}

}
